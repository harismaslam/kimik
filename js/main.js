jQuery.fn.center = function () {
    var ttt1 = $(window).height();
    var ttt4 = ttt1 * 0.35;
    var ttt5;
    var ttt2 = jQuery('.vc-wrap').height();
    var ttt3 = ttt2 - ttt1;
    if (ttt3 < 0) {
        ttt3 = ttt3 * -1;
    }
    if (ttt3 > ttt4) {
        ttt5 = ttt4;
    } else {
        ttt5 = ttt3;
    }
    jQuery('.vc-wrap').css("padding-top", (ttt5 / 2) + 'px');
}
jQuery(document).ready(function () {
    $('.vc-wrap').center();
});
jQuery(window).resize(function () {
    $('.vc-wrap').center();
});
